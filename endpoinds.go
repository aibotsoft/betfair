package betfair

import (
	"context"
	"fmt"
)

// ListCompetitions to get competitions associated with the markets selected by the filter
func (b *Betfair) ListCompetitions(ctx context.Context, filter Filter) (res CompetitionResultList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listCompetitions", &filter)
	return
}

// ListCountries to get a list of countries associated with the markets selected by the filter
func (b *Betfair) ListCountries(ctx context.Context, filter Filter) (res CountryCodeResultList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listCountries", &filter)
	return
}

// ListEventTypes to get a list of all event types
func (b *Betfair) ListEventTypes(ctx context.Context, filter Filter) (res EventTypeResultList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listEventTypes", &filter)
	return
}

// ListEvents to get a list of all events
func (b *Betfair) ListEvents(ctx context.Context, filter Filter) (res EventResultList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listEvents", &filter)
	return
}

// ListMarketCatalogue to get a list of information about published (ACTIVE/SUSPENDED) markets that does not change (or changes very rarely).
func (b *Betfair) ListMarketCatalogue(ctx context.Context, filter Filter) (res MarketCatalogueList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listMarketCatalogue", &filter)
	return
}

// ListMarketBook to get a list of dynamic data about markets.
func (b *Betfair) ListMarketBook(ctx context.Context, filter Filter) (res MarketBookList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listMarketBook", &filter)
	return
}

// ListMarketProfitAndLoss Retrieve profit and loss for a given list of OPEN markets.
func (b *Betfair) ListMarketProfitAndLoss(ctx context.Context, filter Filter) (res MarketProfitAndLossList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listMarketProfitAndLoss", &filter)
	return
}

// ListMarketTypes to get a list of market types (i.e. MATCH_ODDS, NEXT_GOAL) associated with the markets selected by the MarketFilter.
func (b *Betfair) ListMarketTypes(ctx context.Context, filter Filter) (res MarketTypeResultList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listMarketTypes", &filter)
	return
}

// ListTimeRangeResult to get  a list of time ranges in the granularity specified in the request (i.e. 3PM to 4PM, Aug 14th to Aug 15th) associated with the markets selected by the MarketFilter.
func (b *Betfair) ListTimeRangeResult(ctx context.Context, filter Filter) (res TimeRangeResultList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listTimeRanges", &filter)
	return
}

// ListVenueResult to get a list of Venues (i.e. Cheltenham, Ascot) associated with the markets selected by the MarketFilter. Currently, only Horse Racing markets are associated with a Venue.
func (b *Betfair) ListVenueResult(ctx context.Context, filter Filter) (res VenueResultList, err error) {
	err = b.Request(ctx, &res, BettingURL, "listVenues", &filter)
	return
}

func (b *Betfair) CreateAppKeys(ctx context.Context) (res DeveloperAppKey, err error) {
	err = b.Request(ctx, &res, AccountURL, "createDeveloperAppKeys", nil)
	return
}

func (b *Betfair) GetAppKeys(ctx context.Context) (res DeveloperAppKeyList, err error) {
	err = b.Request(ctx, &res, AccountURL, "getDeveloperAppKeys", nil)
	return
}

func (b *Betfair) GetAccountDetails(ctx context.Context) (res AccountDetails, err error) {
	err = b.Request(ctx, &res, AccountURL, "getAccountDetails", nil)
	return
}

func (b *Betfair) GetAccountFunds(ctx context.Context, filter Filter) (res AccountFunds, err error) {
	err = b.Request(ctx, &res, AccountURL, "getAccountFunds", &filter)
	return
}

func (b *Betfair) GetAccountStatement(ctx context.Context, filter Filter) (res AccountStatementReport, err error) {
	err = b.Request(ctx, &res, AccountURL, "getAccountStatement", &filter)
	return
}

func (b *Betfair) GetListCurrencyRates(ctx context.Context, filter Filter) (res CurrencyRateList, err error) {
	err = b.Request(ctx, &res, AccountURL, "listCurrencyRates", &filter)
	return
}

func (b *Betfair) GetTransferFunds(ctx context.Context, filter Filter) (transferResponse TransferResponse, err error) {
	err = b.Request(ctx, &transferResponse, AccountURL, "transferFunds", &filter)
	return
}

func (b *Betfair) PlaceOrders(ctx context.Context, filter Filter) (placeExecutionReport PlaceExecutionReport, err error) {
	err = b.Request(ctx, &placeExecutionReport, BettingURL, "placeOrders", &filter)
	return
}

func (b *Betfair) CancelOrders(ctx context.Context, filter CancelFilter) (cancelExecutionReport CancelExecutionReport, err error) {
	err = b.Request(ctx, &cancelExecutionReport, BettingURL, "cancelOrders", &filter)
	return
}

// ListCurrentOrders to get a list of your current orders
func (b *Betfair) ListCurrentOrders(ctx context.Context, filter Filter) (currentOrderSummaryReport CurrentOrderSummaryReport, err error) {
	err = b.Request(ctx, &currentOrderSummaryReport, BettingURL, "listCurrentOrders", &filter)
	return
}

// ListClearedOrders to get a list of settled bets based on the bet status, ordered by settled date
func (b *Betfair) ListClearedOrders(ctx context.Context, filter Filter) (clearedOrderSummaryReport ClearedOrderSummaryReport, err error) {
	err = b.Request(ctx, &clearedOrderSummaryReport, BettingURL, "listClearedOrders", &filter)
	return
}

// KeepAlive for support connect, session key will available for 20 minutes
func (b *Betfair) KeepAlive(ctx context.Context) error {
	var keepAlive = &KeepAlive{}
	err := b.Request(ctx, keepAlive, KeepAliveURL, "", nil)
	if err != nil {
		return err
	}
	if keepAlive.Status != "SUCCESS" {
		return fmt.Errorf("keepAlive error %s", keepAlive.Error)
	}
	return nil
}

func (b *Betfair) GetSession(ctx context.Context) error {
	var session = &Session{}
	err := b.Request(ctx, session, CertURL, "", nil)
	if err != nil {
		return err
	}
	if session.LoginStatus != "SUCCESS" {
		return fmt.Errorf("session.LoginStatus error: %s", session.LoginStatus)
	}
	b.SessionKey = session.SessionToken
	return nil
}
