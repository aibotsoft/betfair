module gitlab.com/aibotsoft/betfair

go 1.16

require (
	github.com/mailru/easyjson v0.7.7
	github.com/stretchr/testify v1.6.1
	github.com/vrischmann/envconfig v1.3.0
	go.uber.org/zap v1.16.0
	golang.org/x/net v0.0.0-20210226172049-e18ecbb05110 // indirect
)
