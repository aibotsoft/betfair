package betfair

import (
	"context"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/vrischmann/envconfig"
	"go.uber.org/zap"
	"os"
	"testing"
	"time"
)

var bet *Betfair
var log *zap.Logger

func TestMain(m *testing.M) {
	log, _ = zap.NewDevelopment()
	var cfg = &Config{}
	err := envconfig.Init(cfg)
	if err != nil {
		panic(err)
	}
	bet, err = NewBetfair(cfg)
	if err != nil {
		panic(err)
	}
	testToken := os.Getenv("test_token")
	if testToken != "" {
		bet.SessionKey = testToken
	} else {
		err = bet.GetSession(context.Background())
		if err != nil {
			panic(err)
		}
	}
	m.Run()
}

func TestCreateAppKeys(t *testing.T) {
	resp, err := bet.CreateAppKeys(context.Background())
	require.NoError(t, err)
	t.Log(resp)
}
func TestKeepAlive(t *testing.T) {
	err := bet.KeepAlive(context.Background())
	require.NoError(t, err)
}
func TestRequestAppKeys(t *testing.T) {
	keys, err := bet.GetAppKeys(context.Background())
	require.NoError(t, err)
	assert.NotEmpty(t, keys[0].AppID)
	t.Logf("%+v", keys)
}
func TestRequestAccountFunds(t *testing.T) {
	funds, err := bet.GetAccountFunds(context.Background(), Filter{Wallet: W_UK})
	require.NoError(t, err)
	assert.NotEmpty(t, funds.ExposureLimit)
	t.Logf("TestRequestAccountFunds: %+v \n", funds)
}
func TestListCountries(t *testing.T) {
	list, err := bet.ListCountries(context.Background(), Filter{
		//Locale:       "en",
		MarketFilter: &MarketFilter{
			//InPlayOnly: false,
		},
	})
	require.NoError(t, err)
	if bet.Config.Debug {
		t.Logf("TestRequestListCountries\n %+v \n\n", list)
	}
}

func TestRequestAccountDetails(t *testing.T) {
	details, err := bet.GetAccountDetails(context.Background())
	require.NoError(t, err)
	assert.NotEmpty(t, details.CurrencyCode)
	t.Logf("TestRequestAccountDetails: %+v\n", details)
}
func TestRequestListCurrentOrders(t *testing.T) {
	list, err := bet.ListCurrentOrders(context.Background(), Filter{})
	require.NoError(t, err)
	if bet.Config.Debug {
		t.Logf("TestRequestListCurrentOrders:%+v \n\n", list)
	}
}

func TestRequestListClearedOrders(t *testing.T) {
	list, err := bet.ListClearedOrders(context.Background(), Filter{
		SettledDateRange: &DateRange{time.Now().AddDate(-1, 0, 0), time.Now()},
		BetStatus:        BS_SETTLED,
	})
	require.NoError(t, err)
	if bet.Config.Debug {
		t.Logf("TestRequestListClearedOrders\n %+v \n\n", list)
	}
}

func TestRequestListEventTypes(t *testing.T) {
	list, err := bet.ListEventTypes(context.Background(), Filter{
		Locale:       "en",
		MarketFilter: &MarketFilter{},
	})
	require.NoError(t, err)
	if bet.Config.Debug {
		t.Logf("TestRequestListEventTypes\n%+v \n\n", list)
	}
}

func TestRequestListCompetitions(t *testing.T) {
	list, err := bet.ListCompetitions(context.Background(), Filter{
		Locale:       "en",
		MarketFilter: &MarketFilter{
			//InPlayOnly: false,
		},
	})
	require.NoError(t, err)
	if bet.Config.Debug {
		t.Logf("TestRequestListCompetitions:\n%+v \n\n", list)
	}
}

func TestRequestListEvents(t *testing.T) {
	list, err := bet.ListEvents(context.Background(), Filter{
		Locale: "en",
		MarketFilter: &MarketFilter{
			EventTypeIDs: []string{"1"},
			MarketStartTime: &DateRange{
				From: time.Now(),
				To:   time.Now().Add(time.Hour * 9)},
		},
	})
	require.NoError(t, err)
	if bet.Config.Debug {
		t.Logf("TestRequestListEvents\n %+v \n\n", list)
	}
}

func TestListMarketCatalogue(t *testing.T) {
	list, err := bet.ListMarketCatalogue(context.Background(), Filter{
		Locale: "en",
		MarketFilter: &MarketFilter{
			EventTypeIDs: []string{"1"},
			//MarketTypeCodes: []string{"MATCH_ODDS"},
			MarketTypeCodes: []string{"ASIAN_HANDICAP"},
			MarketStartTime: &DateRange{
				From: time.Now(),
				To:   time.Now().Add(time.Hour * 1)},
		},
		MarketProjection: &[]EMarketProjection{
			COMPETITION,
			EVENT,
			//EVENT_TYPE,
			MARKET_START_TIME,
			//Weight=1
			//MARKET_DESCRIPTION,
			//Weight=1
			//RUNNER_DESCRIPTION,
			//RUNNER_METADATA,
		},
		MaxResults: 10,
	})
	require.NoError(t, err)
	if bet.Config.Debug {
		log.Debug("TestListMarketCatalogue", zap.Any("", list), zap.Int("count", len(list)))
	}
}
func TestListMarketBook(t *testing.T) {
	list, err := bet.ListMarketBook(context.Background(), Filter{
		CurrencyCode: "EUR",
		MarketIDs:    []string{"1.180289429"},
		PriceProjection: &PriceProjection{
			PriceData: []EPriceData{PD_EX_BEST_OFFERS},
		},
		//MarketFilter: &MarketFilter{
		//	EventTypeIDs:    []string{"1"},
		//	MarketTypeCodes: []string{"MATCH_ODDS"},
		//	MarketStartTime: &DateRange{
		//		From: time.Now(),
		//		To:   time.Now().Add(time.Hour * 9)},
		//},
		//MarketProjection: &[]EMarketProjection{
		//	COMPETITION,
		//	EVENT,
		//	EVENT_TYPE,
		//	MARKET_START_TIME,
		//	//Weight=1
		//	//MARKET_DESCRIPTION,
		//	//Weight=1
		//	//RUNNER_DESCRIPTION,
		//	//RUNNER_METADATA,
		//},
	})
	require.NoError(t, err)
	if bet.Config.Debug {
		log.Debug("TestListMarketBook", zap.Any("", list), zap.Int("count", len(list)))
	}
}
func TestListMarketTypes(t *testing.T) {
	list, err := bet.ListMarketTypes(context.Background(), Filter{
		Locale:       "en",
		MarketFilter: &MarketFilter{},
	})
	require.NoError(t, err)
	if bet.Config.Debug {
		t.Logf("TestRequestListMarketTypes\n %v \n\n", list)
	}
}

func TestListCurrencyRates(t *testing.T) {
	list, err := bet.GetListCurrencyRates(context.Background(), Filter{})
	require.NoError(t, err)
	if bet.Config.Debug {
		log.Debug("TestListCurrencyRates", zap.Any("", list), zap.Int("count", len(list)))
	}
}

func BenchmarkRequestKeys(b *testing.B) {
	for n := 0; n < b.N; n++ {
		_, err := bet.GetAppKeys(context.Background())
		if err != nil {
			b.Error(err)
		}
	}
}

func TestRequestPlaceOrders(t *testing.T) {
	list, err := bet.PlaceOrders(context.Background(), Filter{
		PlaceOrdersMarketID: "1.139017587",
		PlaceOrdersInstructions: []PlaceInstruction{
			PlaceInstruction{
				SelectionID: 256171,
				Side:        S_LAY,
				Handicap:    0,
				OrderType:   OT_MARKET_ON_CLOSE,
				//MarketOnCloseOrder: MarketOnCloseOrder{
				//	Liability: 1,
				//},
			},
		},
	})
	require.NoError(t, err)
	if bet.Config.Debug {
		t.Logf("TestRequestPlaceOrders\n %v \n\n", list)
	}
}

func TestRequestListMarketProfitAndLoss(t *testing.T) {
	list, err := bet.ListMarketProfitAndLoss(context.Background(), Filter{
		MarketIDs: []string{"1.125821194"},
	})
	if err != nil {
		t.Error(err)
	}

	if bet.Config.Debug {
		t.Logf("TestRequestListMarketProfitAndLoss\n %v \n\n", list)
	}
}

func TestRequestListTimeRangeResult(t *testing.T) {
	list, err := bet.ListTimeRangeResult(context.Background(), Filter{
		MarketFilter:    &MarketFilter{MarketIDs: []string{"1.125821194"}},
		TimeGranularity: TG_DAYS,
	})
	if err != nil {
		t.Error(err)
	}

	if bet.Config.Debug {
		t.Logf("TestRequestListTimeRangeResult\n %v \n\n", list)
	}
}

func TestRequestListVenueResult(t *testing.T) {
	list, err := bet.ListVenueResult(context.Background(), Filter{
		MarketFilter: &MarketFilter{MarketIDs: []string{"1.125821194"}},
		Locale:       "en",
	})
	if err != nil {
		t.Error(err)
	}

	if bet.Config.Debug {
		t.Logf("TestRequestListVenueResult\n %v \n\n", list)
	}
}
