package betfair

import (
	"fmt"
	"time"
)

type KeepAlive struct {
	Token   string `json:"token"`
	Product string `json:"product"`
	Status  string `json:"status"`
	Error   string `json:"error"`
}

type DateRange struct {
	From time.Time `json:"from,omitempty"`
	To   time.Time `json:"to,omitempty"`
}
type Session struct {
	SessionToken string       `json:"sessionToken"`
	LoginStatus  ELoginStatus `json:"loginStatus"`
}

type RunnerID struct {
	MarketID    string
	SelectionID int64
	Handicap    float64
}

type EXBestOffersOverrides struct {
	BestPricesDepth          int64        `json:"bestPricesDepth,omitempty"`
	RollupModel              ERollupModel `json:"rollupModel,omitempty"`
	RollupLimit              int64        `json:"rollupLimit,omitempty"`
	RollupLiabilityThreshold float64      `json:"rollupLiabilityThreshold,omitempty"`
	RollupLiabilityFactor    int64        `json:"rollupLiabilityFactor,omitempty"`
}

type PriceProjection struct {
	PriceData             []EPriceData           `json:"priceData,omitempty"`
	EXBestOffersOverrides *EXBestOffersOverrides `json:"exBestOffersOverrides,omitempty"`
	Virtualise            bool                   `json:"virtualise,omitempty"`
	RolloverStakes        bool                   `json:"rolloverStakes,omitempty"`
}

type Filter struct {
	Wallet                  EWallet              `json:"wallet,omitempty"`
	CurrencyCode            string               `json:"currencyCode,omitempty"`
	Locale                  string               `json:"locale,omitempty"`
	FromRecord              int64                `json:"fromRecord,omitempty"`
	RecordCount             int64                `json:"recordCount,omitempty"`
	ItemDateRange           *DateRange           `json:"itemDateRange,omitempty"`
	IncludeItem             EIncludeItem         `json:"includeItem,omitempty"`
	FromCurrency            string               `json:"fromCurrency,omitempty"`
	From                    EWallet              `json:"from,omitempty"`
	To                      EWallet              `json:"to,omitempty"`
	Amount                  float64              `json:"amount,omitempty"`
	BetIDs                  []string             `json:"betIds,omitempty"`
	MarketIDs               []string             `json:"marketIds,omitempty"`
	PriceProjection         *PriceProjection     `json:"priceProjection,omitempty"`
	OrderProjection         EOrderProjection     `json:"orderProjection,omitempty"`
	MarketProjection        *[]EMarketProjection `json:"marketProjection,omitempty"`
	DateRange               *DateRange           `json:"dateRange,omitempty"`
	OrderBy                 EOrderBy             `json:"orderBy,omitempty"`
	SortDir                 ESortDir             `json:"sortDir,omitempty"`
	Sort                    EMarketSort          `json:"sort,omitempty"`
	MarketFilter            *MarketFilter        `json:"filter,omitempty"`
	BetStatus               EBetStatus           `json:"betStatus,omitempty"`
	EventTypeIDs            []string             `json:"eventTypeIds,omitempty"`
	EventIDs                []string             `json:"eventIds,omitempty"`
	RunnerIDs               []RunnerID           `json:"runnerIds,omitempty"`
	Side                    ESide                `json:"side,omitempty"`
	SettledDateRange        *DateRange           `json:"settledDateRange,omitempty"`
	GroupBy                 EGroupBy             `json:"groupBy,omitempty"`
	IncludeItemDescription  bool                 `json:"includeItemDescription,omitempty"`
	MaxResults              int64                `json:"maxResults,omitempty"`
	IncludeSettledBets      bool                 `json:"includeSettledBets,omitempty"`
	TimeGranularity         ETimeGranularity     `json:"granularity,omitempty"`
	PlaceOrdersMarketID     string               `json:"marketId,omitempty"`
	PlaceOrdersInstructions []PlaceInstruction   `json:"instructions,omitempty"`
	CustomerOrderRefs       []string             `json:"customerOrderRefs,omitempty"`
	CustomerStrategyRefs    []string             `json:"customerStrategyRefs,omitempty"`
}

type CancelFilter struct {
	MarketID                 string              `json:"marketId"`
	CancelOrdersInstructions []CancelInstruction `json:"instructions"`
	CustomerRef              string              `json:"customerRef,omitempty"`
}
type MarketFilter struct {
	TextQuery          string               `json:"textQuery,omitempty"`
	ExchangeIDs        []string             `json:"exchangeIds,omitempty"`
	EventTypeIDs       []string             `json:"eventTypeIds,omitempty"`
	EventIDs           []string             `json:"eventIds,omitempty"`
	CompetitionIDs     []string             `json:"competitionIds,omitempty"`
	MarketIDs          []string             `json:"marketIds,omitempty"`
	Venues             []string             `json:"venues,omitempty"`
	BspOnly            *bool                `json:"bspOnly,omitempty"`
	TurnInPlayEnabled  *bool                `json:"turnInPlayEnabled,omitempty"`
	InPlayOnly         *bool                `json:"inPlayOnly,omitempty"`
	MarketBettingTypes []EMarketBettingType `json:"marketBettingTypes,omitempty"`
	MarketCountries    []string             `json:"marketCountries,omitempty"`
	MarketTypeCodes    []string             `json:"marketTypeCodes,omitempty"`
	MarketStartTime    *DateRange           `json:"marketStartTime,omitempty"`
	WithOrders         []EOrderStatus       `json:"withOrders,omitempty"`
}

type DeveloperAppVersion struct {
	Owner                string `json:"owner"`
	VersionID            int64  `json:"versionId"`
	Version              string `json:"version"`
	ApplicationKey       string `json:"applicationKey"`
	DelayData            bool   `json:"delayData"`
	SubscriptionRequired bool   `json:"subscriptionRequired"`
	OwnerManaged         bool   `json:"ownerManaged"`
	Active               bool   `json:"active"`
}

type DeveloperAppKey struct {
	AppName     string                `json:"appName"`
	AppID       int64                 `json:"appId"`
	AppVersions []DeveloperAppVersion `json:"appVersions"`
}

//easyjson:json
type DeveloperAppKeyList []DeveloperAppKey

type AccountDetails struct {
	CurrencyCode  string  `json:"currencyCode"`
	FirstName     string  `json:"firstName"`
	LastName      string  `json:"lastName"`
	LocaleCode    string  `json:"localeCode"`
	Region        string  `json:"region"`
	Timezone      string  `json:"timezone"`
	DiscountRate  float64 `json:"discountRate"`
	PointsBalance int64   `json:"pointsBalance"`
	CountryCode   string  `json:"countryCode"`
}

type AccountFunds struct {
	AvailableToBetBalance float64 `json:"availableToBetBalance"`
	Exposure              float64 `json:"exposure"`
	RetainedCommission    float64 `json:"retainedCommission"`
	ExposureLimit         float64 `json:"exposureLimit"`
	DiscountRate          float64 `json:"discountRate"`
	PointsBalance         int64   `json:"pointsBalance"`
	Wallet                string  `json:"wallet"`
}

type StatementLegacyData struct {
	AvgPrice        float64
	BetSize         float64
	BetType         string
	BetCategoryType string
	CommissionRate  string
	EventID         int64
	EventTypeID     int64
	FullMarketName  string
	GrossBetAmount  float64
	MarketName      string
	MarketType      EMarketType
	PlacedDate      time.Time
	SelectionID     int64
	SelectionName   string
	StartDate       time.Time
	TransactionType string
	TransactionID   int64
	WinLose         string
}

type StatementItem struct {
	RefID         string
	ItemDate      time.Time
	Amount        float64
	Balance       float64
	ItemClass     EItemClass
	ItemClassData map[string]string
	LegacyData    StatementLegacyData
}

type CurrencyRate struct {
	CurrencyCode string
	Rate         float64
}

//easyjson:json
type CurrencyRateList []CurrencyRate

type AccountStatementReport struct {
	AccountStatement []StatementItem
	MoreAvailable    bool
}

type TransferResponse struct {
	TransactionID string
}

type MarketCatalogue struct {
	MarketID        string             `json:"marketId"`
	MarketName      string             `json:"marketName"`
	MarketStartTime *time.Time         `json:"marketStartTime,omitempty"`
	Description     *MarketDescription `json:"description,omitempty"`
	TotalMatched    float64            `json:"totalMatched"`
	Runners         *[]RunnerCatalog   `json:"runners,omitempty"`
	EventType       *EventType         `json:"eventType,omitempty"`
	Competition     *Competition       `json:"competition,omitempty"`
	Event           *Event             `json:"event,omitempty"`
}

//easyjson:json
type MarketCatalogueList []MarketCatalogue

type MarketDescription struct {
	PersistenceEnabled bool               `json:"persistenceEnabled,omitempty"`
	BspMarket          bool               `json:"bspMarket,omitempty"`
	MarketTime         time.Time          `json:"marketTime,omitempty"`
	SuspendTime        time.Time          `json:"suspendTime,omitempty"`
	SettleTime         time.Time          `json:"settleTime,omitempty"`
	BettingType        EMarketBettingType `json:"bettingType,omitempty"`
	TurnInPlayEnabled  bool               `json:"turnInPlayEnabled,omitempty"`
	MarketType         string             `json:"marketType,omitempty"`
	Regulator          string             `json:"marketId,omitempty"`
	MarketBaseRate     float64            `json:"marketBaseRate,omitempty"`
	DiscountAllowed    bool               `json:"discountAllowed,omitempty"`
	Wallet             string             `json:"wallet,omitempty"`
	Rules              string             `json:"rules,omitempty"`
	RulesHasDate       bool               `json:"rulesHasDate,omitempty"`
	EachWayDivisor     float64            `json:"eachWayDivisor,omitempty"`
	Clarifications     string             `json:"clarifications,omitempty"`
}

type RunnerCatalog struct {
	SelectionID  int64             `json:"selectionId,omitempty"`
	RunnerName   string            `json:"runnerName,omitempty"`
	Handicap     float64           `json:"handicap,omitempty"`
	SortPriority int64             `json:"sortPriority,omitempty"`
	Metadata     map[string]string `json:"metadata,omitempty"`
}

type MarketBook struct {
	MarketID              string        `json:"marketId"`
	IsMarketDataDelayed   bool          `json:"isMarketDataDelayed"`
	Status                EMarketStatus `json:"status"`
	BetDelay              int64         `json:"betDelay"`
	BspReconciled         bool          `json:"bspReconciled"`
	Complete              bool          `json:"complete"`
	Inplay                bool          `json:"inplay"`
	NumberOfWinners       int64         `json:"numberOfWinners"`
	NumberOfRunners       int64         `json:"numberOfRunners"`
	NumberOfActiveRunners int64         `json:"numberOfActiveRunners"`
	LastMatchTime         time.Time     `json:"lastMatchTime"`
	TotalMatched          float64       `json:"totalMatched"`
	TotalAvailable        float64       `json:"totalAvailable"`
	CrossMatching         bool          `json:"crossMatching"`
	RunnersVoidable       bool          `json:"runnersVoidable"`
	Version               int64         `json:"version"`
	Runners               []Runner      `json:"runners"`
}

//easyjson:json
type MarketBookList []MarketBook
type Runner struct {
	SelectionID       int64              `json:"selectionId"`
	Handicap          float64            `json:"handicap,omitempty"`
	Status            ERunnerStatus      `json:"status,omitempty"`
	AdjustmentFactor  float64            `json:"adjustmentFactor,omitempty"`
	LastPriceTraded   float64            `json:"lastPriceTraded"`
	TotalMatched      float64            `json:"totalMatched,omitempty"`
	RemovalDate       *time.Time         `json:"removalDate,omitempty"`
	SP                *StartingPrices    `json:"sp,omitempty"`
	EX                ExchangePrices     `json:"ex"`
	Orders            []Order            `json:"orders,omitempty"`
	Matches           []Match            `json:"matches,omitempty"`
	MatchesByStrategy map[string][]Match `json:"matchesByStrategy,omitempty"`
}

type StartingPrices struct {
	NearPrice         float64     `json:"nearPrice,omitempty"`
	FarPrice          float64     `json:"farPrice,omitempty"`
	BackStakeTaken    []PriceSize `json:"backStakeTaken,omitempty"`
	LayLiabilityTaken []PriceSize `json:"layLiabilityTaken,omitempty"`
	ActualSP          float64     `json:"actualSP,omitempty"`
}

type ExchangePrices struct {
	AvailableToBack []PriceSize `json:"availableToBack,omitempty"`
	AvailableToLay  []PriceSize `json:"availableToLay,omitempty"`
	TradedVolume    []PriceSize `json:"tradedVolume,omitempty"`
}

type Order struct { /* TODO: Implement */
}

type Match struct { /* TODO: Implement */
}
type MarketProfitAndLoss struct {
	MarketId          string                `json:"marketId,omitempty"`
	CommissionApplied float64               `json:"commissionApplied,omitempty"`
	ProfitAndLosses   []RunnerProfitAndLoss `json:"profitAndLosses,omitempty"`
}

//easyjson:json
type MarketProfitAndLossList []MarketProfitAndLoss

type RunnerProfitAndLoss struct {
	SelectionId int64   `json:"selectionId,omitempty"`
	IfWin       float64 `json:"ifWin,omitempty"`
	IfLose      float64 `json:"ifLose,omitempty"`
	IfPlace     float64 `json:"ifPlace,omitempty"`
}

type MarketTypeResult struct {
	MarketType  string `json:"marketType,omitempty"`
	MarketCount int64  `json:"marketCount,omitempty"`
}

//easyjson:json
type MarketTypeResultList []MarketTypeResult

type TimeRangeResult struct {
	TimeRange   DateRange `json:"timeRange,omitempty"`
	MarketCount int64     `json:"marketCount,omitempty"`
}

//easyjson:json
type TimeRangeResultList []TimeRangeResult

type VenueResult struct {
	Venue       string `json:"venue,omitempty"`
	MarketCount int64  `json:"marketCount,omitempty"`
}

//easyjson:json
type VenueResultList []VenueResult

type PriceSize struct {
	Price float64 `json:"price,omitempty"`
	Size  float64 `json:"size,omitempty"`
}

type CurrentOrderSummary struct {
	BetID               string           `json:"betId"`
	MarketID            string           `json:"marketId"`
	SelectionID         int64            `json:"selectionId"`
	Handicap            float64          `json:"handicap"`
	PriceSize           PriceSize        `json:"priceSize"`
	BspLiability        float64          `json:"bspLiability"`
	Side                ESide            `json:"side"`
	Status              EOrderStatus     `json:"status"`
	PersistenceType     EPersistenceType `json:"persistenceType"`
	OrderType           EOrderType       `json:"orderType"`
	PlacedDate          time.Time        `json:"placedDate"`
	MatchedDate         time.Time        `json:"matchedDate"`
	AveragePriceMatched float64          `json:"averagePriceMatched,omitempty"`
	SizeMatched         float64          `json:"sizeMatched,omitempty"`
	SizeRemaining       float64          `json:"sizeRemaining,omitempty"`
	SizeLapsed          float64          `json:"sizeLapsed,omitempty"`
	SizeCancelled       float64          `json:"sizeCancelled,omitempty"`
	SizeVoided          float64          `json:"sizeVoided,omitempty"`
	RegulatorAuthCode   string           `json:"regulatorAuthCode,omitempty"`
	RegulatorCode       string           `json:"regulatorCode,omitempty"`
}

type CurrentOrderSummaryReport struct {
	CurrentOrders []CurrentOrderSummary `json:"currentOrders"`
	MoreAvailable bool                  `json:"moreAvailable"`
}

type ItemDescription struct {
	EventTypeDesc   string    `json:"eventTypeDesc,omitempty"`
	EventDesc       string    `json:"eventDesc,omitempty"`
	MarketDesc      string    `json:"marketDesc,omitempty"`
	MarketType      string    `json:"marketType,omitempty"`
	MarketStartTime time.Time `json:"marketStartTime,omitempty"`
	RunnerDesc      string    `json:"runnerDesc,omitempty"`
	NumberOfWinners int64     `json:"numberOfWinners,omitempty"`
	EachWayDivisor  float64   `json:"eachWayDivisor,omitempty"`
}

type ClearedOrderSummary struct {
	EventTypeID         string           `json:"eventTypeId,omitempty"`
	EventID             string           `json:"eventId,omitempty"`
	MarketID            string           `json:"marketId,omitempty"`
	SelectionID         int64            `json:"selectionId,omitempty"`
	Handicap            float64          `json:"handicap,omitempty"`
	BetID               string           `json:"betId,omitempty"`
	PlacedDate          time.Time        `json:"placedDate,omitempty"`
	PersistenceType     EPersistenceType `json:"persistenceType,omitempty"`
	OrderType           EOrderType       `json:"orderType,omitempty"`
	Side                ESide            `json:"side,omitempty"`
	ItemDescription     ItemDescription  `json:"itemDescription,omitempty"`
	BetOutcome          string           `json:"betOutcome,omitempty"`
	PriceRequested      float64          `json:"priceRequested,omitempty"`
	SettledDate         time.Time        `json:"settledDate,omitempty"`
	LastMatchedDate     time.Time        `json:"lastMatchedDate,omitempty"`
	BetCount            int64            `json:"betCount,omitempty"`
	Commission          float64          `json:"commission,omitempty"`
	PriceMatched        float64          `json:"priceMatched,omitempty"`
	PriceReduced        bool             `json:"priceReduced,omitempty"`
	SizeSettled         float64          `json:"sizeSettled,omitempty"`
	Profit              float64          `json:"profit,omitempty"`
	SizeCancelled       float64          `json:"sizeCancelled,omitempty"`
	CustomerOrderRef    string           `json:"customerOrderRef,omitempty"`
	CustomerStrategyRef string           `json:"customerStrategyRef,omitempty"`
}

type ClearedOrderSummaryReport struct {
	ClearedOrders []ClearedOrderSummary `json:"clearedOrders"`
	MoreAvailable bool                  `json:"moreAvailable"`
}

type Event struct {
	ID          string    `json:"id,omitempty"`
	Name        string    `json:"name,omitempty"`
	CountryCode string    `json:"countryCode,omitempty"`
	Timezone    string    `json:"timezone,omitempty"`
	Venue       string    `json:"venue,omitempty"`
	OpenDate    time.Time `json:"openDate,omitempty"`
}

type EventResult struct {
	Event       Event `json:"event"`
	MarketCount int64 `json:"marketCount"`
}

//easyjson:json
type EventResultList []EventResult

type EventType struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type EventTypeResult struct {
	EventType   EventType `json:"eventType"`
	MarketCount int64     `json:"marketCount"`
}

//easyjson:json
type EventTypeResultList []EventTypeResult

type Competition struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type CompetitionResult struct {
	Competition       Competition `json:"competition"`
	MarketCount       int64       `json:"marketCount"`
	CompetitionRegion string      `json:"competitionRegion"`
}

//easyjson:json
type CompetitionResultList []CompetitionResult

type CountryCodeResult struct {
	CountryCode string `json:"countryCode"`
	MarketCount int64  `json:"marketCount"`
}

//easyjson:json
type CountryCodeResultList []CountryCodeResult

type Decimal float64

func (n Decimal) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf("%.2f", n)), nil
}

type PlaceInstruction struct {
	OrderType          EOrderType          `json:"orderType,omitempty"`
	SelectionID        int64               `json:"selectionId,omitempty"`
	Handicap           Decimal             `json:"handicap,omitempty"`
	Side               ESide               `json:"side,omitempty"`
	LimitOrder         *LimitOrder         `json:"limitOrder,omitempty"`
	LimitOnCloseOrder  *LimitOnCloseOrder  `json:"limitOnCloseOrder,omitempty"`
	MarketOnCloseOrder *MarketOnCloseOrder `json:"marketOnCloseOrder,omitempty"`
	CustomerOrderRef   string              `json:"customerOrderRef,omitempty"`
}

type PlaceExecutionReport struct {
	CustomerRef        string                    `json:"customerRef,omitempty"`
	Status             EExecutionReportStatus    `json:"status,omitempty"`
	ErrorCode          EExecutionReportErrorCode `json:"errorCode,omitempty"`
	MarketID           string                    `json:"marketId,omitempty"`
	InstructionReports []PlaceInstructionReport  `json:"instructionReports,omitempty"`
}

type PlaceInstructionReport struct {
	Status              EInstructionReportStatus    `json:"status,omitempty"`
	ErrorCode           EInstructionReportErrorCode `json:"errorCode,omitempty"`
	OrderStatus         EOrderStatus                `json:"orderStatus,omitempty"`
	Instruction         PlaceInstruction            `json:"instruction,omitempty"`
	BetID               string                      `json:"betId,omitempty"`
	PlacedDate          time.Time                   `json:"placedDate,omitempty"`
	AveragePriceMatched float64                     `json:"averagePriceMatched,omitempty"`
	SizeMatched         float64                     `json:"sizeMatched,omitempty"`
}

type LimitOrder struct {
	Size            Decimal          `json:"size,omitempty"`
	Price           Decimal          `json:"price,omitempty"`
	PersistenceType EPersistenceType `json:"persistenceType,omitempty"`
	TimeInForce     ETimeInForce     `json:"timeInForce,omitempty"`
	MinFillSize     Decimal          `json:"minFillSize,omitempty"`
	BetTargetType   EBetTargetType   `json:"betTargetType,omitempty"`
	BetTargetSize   Decimal          `json:"betTargetSize,omitempty"`
}

type LimitOnCloseOrder struct {
	Liability Decimal `json:"liability,omitempty"`
	Price     Decimal `json:"price,omitempty"`
}

type MarketOnCloseOrder struct {
	Liability Decimal `json:"liability,omitempty"`
}

type CancelInstruction struct {
	BetID         string  `json:"betId"`
	SizeReduction Decimal `json:"sizeReduction,omitempty"`
}

type CancelExecutionReport struct {
	CustomerRef        string                    `json:"customerRef,omitempty"`
	Status             EExecutionReportStatus    `json:"status,omitempty"`
	ErrorCode          EExecutionReportErrorCode `json:"errorCode,omitempty"`
	MarketID           string                    `json:"marketId,omitempty"`
	InstructionReports []CancelInstructionReport `json:"instructionReports,omitempty"`
}

type CancelInstructionReport struct {
	Status        EInstructionReportStatus    `json:"status,omitempty"`
	ErrorCode     EInstructionReportErrorCode `json:"errorCode,omitempty"`
	Instruction   CancelInstruction           `json:"instruction,omitempty"`
	CancelledDate time.Time                   `json:"cancelledDate,omitempty"`
	SizeCancelled float64                     `json:"sizeCancelled,omitempty"`
}

var bettingError = BettingError{}

type BettingError struct {
	Detail      interface{} `json:"detail"`
	Faultcode   string      `json:"faultcode"`
	Faultstring string      `json:"faultstring"`
}
