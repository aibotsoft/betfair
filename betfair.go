package betfair

import (
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/mailru/easyjson"
	"golang.org/x/net/http2"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"time"
)

type Config struct {
	ApiKey    string `envconfig:"api_key"`
	Login     string `json:"login"`
	Password  string `json:"password"`
	CertPem   string `json:"cert_pem"`
	CertKey   string `json:"cert_key"`
	UserAgent string `json:"user_agent"`
	Debug     bool   `envconfig:"default=false"`
}

type Betfair struct {
	Config     *Config
	Client     *http.Client
	SessionKey string
}

func NewBetfair(config *Config) (*Betfair, error) {
	cert, err := tls.LoadX509KeyPair(config.CertPem, config.CertKey)
	if err != nil {
		return nil, fmt.Errorf("LoadX509KeyPair error: %w", err)
	}
	tr := &http.Transport{
		DisableCompression: false,
		TLSClientConfig:    &tls.Config{Certificates: []tls.Certificate{cert}},
	}
	err = http2.ConfigureTransport(tr)
	if err != nil {
		return nil, fmt.Errorf("http2.ConfigureTransport error: %w", err)
	}
	return &Betfair{Config: config, Client: &http.Client{Transport: tr}}, nil
}

// Request function for send requests to betfair via REST JSON
func (b *Betfair) Request(ctx context.Context, target easyjson.Unmarshaler, url BetfairRestURL, method string, filter interface{}) error {
	urlBuild := new(bytes.Buffer)
	urlBuild.WriteString(string(url))
	urlBuild.WriteString("/")
	if method != "" {
		urlBuild.WriteString(method)
		urlBuild.WriteString("/")
	}
	payloadBuf := new(bytes.Buffer)
	if filter != nil {
		err := json.NewEncoder(payloadBuf).Encode(filter)
		if err != nil {
			return fmt.Errorf("encode post data error: %w", err)
		}
	}
	if url == CertURL {
		payloadBuf.WriteString(`username=`)
		payloadBuf.WriteString(b.Config.Login)
		payloadBuf.WriteString(`&password=`)
		payloadBuf.WriteString(b.Config.Password)
	}

	req, err := http.NewRequestWithContext(ctx, "POST", urlBuild.String(), payloadBuf)
	if err != nil {
		return err
	}
	if url == CertURL {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	} else {
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("X-Authentication", b.SessionKey)
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Add("User-Agent", b.Config.UserAgent)
	req.Header.Set("Connection", "keep-alive")
	req.Header.Set("X-Application", b.Config.ApiKey)

	if b.Config.Debug {
		dump, err := httputil.DumpRequestOut(req, true)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s\n\n", dump)
	}
	start := time.Now()
	resp, err := b.Client.Do(req)
	if err != nil {
		return fmt.Errorf("reques error: %w", err)
	}
	defer func() {
		err2 := resp.Body.Close()
		if err2 != nil {
			fmt.Println("close body error", err2)
		}
	}()
	if b.Config.Debug {
		fmt.Printf("TotalTime: %s\n", time.Since(start))
		fmt.Println("Uncompressed:", resp.Uncompressed)
		dump, err := httputil.DumpResponse(resp, true)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%s\n\n", dump)

	}

	if resp.StatusCode == 400 {
		var bettingError BettingError
		err = easyjson.UnmarshalFromReader(resp.Body, &bettingError)
		if err != nil {
			return fmt.Errorf("unmarshal bettingError error: %w", err)
		}
		return fmt.Errorf("error: faultcode: %s, faultstring: %s, detail: %+v",
			bettingError.Faultcode, bettingError.Faultstring, bettingError.Detail)
	}
	unmarshalStart := time.Now()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	err = easyjson.Unmarshal(body, target)
	if err != nil {
		return fmt.Errorf("unmarshal response error: %w", err)
	}

	if b.Config.Debug {
		fmt.Printf("UnmarshalTime: %s, datalen:%d\n", time.Since(unmarshalStart), len(body))
	}
	return nil
}
